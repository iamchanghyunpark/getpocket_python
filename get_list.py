import json
import keys
import get_auth
import retrieve

def get_first_ten(auth):
    result_json = retrieve.get_list(auth, {'count':10})

    print(json.dumps(result_json, indent=2)) 

def archive_old_stuff(auth):
    ids = []
    action_array = []
    opt_params = {
            'state':'unread',
            'count':5000,
            'offset':24,
            'detailType':'simple',
            'sort':'newest',

        }
    last_return = 1
    while last_return != 0:
        result_json = retrieve.get_list(auth, opt_params)
        last_return = len(result_json)
        print("Got results from %d to %d" % (opt_params['offset'], opt_params['offset'] + last_return))
        opt_params['offset'] += last_return
        if last_return == 0:
            break
        for key in result_json.keys():
            #print(key, result_json[key]['resolved_title'])
            ids.append(int(key))

    total_to_archive = len(ids)
    print("modifying %d articles" % total_to_archive)

    for article_id in ids:
        action_array.append({
            'action': 'archive',
            'item_id': str(article_id)
            })
    print(len(ids))
    print(len(action_array))

    batch_nr = 1000

    for i in range(0, total_to_archive, batch_nr):
        count = total_to_archive - i
        if count > batch_nr:
            count = batch_nr 
        action_params = {
                'actions': action_array[i:i+count]
                }
        print("Sending frequest from %d to %d ids" % (i, i+count))
        #print(json.dumps(action_params, indent=2))
        result_json = retrieve.modify_list(auth, action_params)

def count_total_articles(auth):
    opt_params = {
            'state':'all',
            'count':5000,
            'offset':0,
            'detailType':'simple',
            'sort':'newest',
            }
    last_return = 1
    count_articles = 0
    while last_return != 0:
        result_json = retrieve.get_list(auth, opt_params)
        last_return = len(result_json)
        opt_params['offset'] += last_return
        count_articles += last_return

    print("You have a total of %d articles" % count_articles)


def get_unread_articles(auth):
    opt_params = {
            'state':'unread',
            'count':1000,
            'offset':0,
            'detailType':'simple',
            'sort':'newest',

        }
    last_return = 1
    while last_return != 0:
        result_json = retrieve.get_list(auth, opt_params)
        #print(json.dumps(result_json, indent=2)) 
        last_return = len(result_json)
        print("Got results from %d to %d" % (opt_params['offset'], opt_params['offset'] + last_return))
        opt_params['offset'] += last_return
        #for key in sorted(result_json.keys(), key=lambda key: int(key), reverse=True):
        for key in result_json.keys():
            print(key, result_json[key]['resolved_title'])

def get_articles_with_tag(auth, tag):
    opt_params = {
            'state':'unread',
            'count':1000,
            'offset':0,
            'detailType':'simple',
            'sort':'newest',
            'tag':tag
        }
    last_return = 1
    while last_return != 0:
        result_json = retrieve.get_list(auth, opt_params)
        #print(json.dumps(result_json, indent=2)) 
        last_return = len(result_json)
        print("Got results from %d to %d" % (opt_params['offset'], opt_params['offset'] + last_return))
        opt_params['offset'] += last_return
        #for key in sorted(result_json.keys(), key=lambda key: int(key), reverse=True):
        if last_return == 0:
            break
        for key in result_json.keys():
            print(key, result_json[key]['resolved_title'])

if __name__ == '__main__':
    auth = get_auth.auth()
    #get_first_ten(auth)
    #get_unread_articles(auth)
    #get_articles_with_tag(auth, 'deep learning')
    #archive_old_stuff(auth)
    count_total_articles(auth)

