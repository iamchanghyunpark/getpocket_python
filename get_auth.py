import json
import keys

def auth():
    auth_credentials = keys.check_and_load()
    return auth_credentials

if __name__ == '__main__':
    auth_credentials = auth()
    print(auth_credentials)
