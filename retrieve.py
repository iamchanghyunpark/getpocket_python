import json
import requests
from urllib.parse import urlencode
from urllib.request import Request, urlopen, urlretrieve

def get_list(auth, opt_params):
    get_url = 'https://getpocket.com/v3/get'

    #post_fields = auth + opt_params
    # https://stackoverflow.com/a/26853961
    post_fields = {**auth, **opt_params}
    request = Request(get_url, urlencode(post_fields).encode())
    response = urlopen(request).read().decode()

    response_json = json.loads(response)

    result = response_json['list']
    return result

def modify_list(auth, action_params):
    get_url = 'https://getpocket.com/v3/send'

    #post_fields = auth + opt_params
    # https://stackoverflow.com/a/26853961
    post_fields = {**auth, **action_params}
    #print(json.dumps(post_fields, indent=2))
    encoded = urlencode(post_fields)
    encoded_subst = encoded.replace('%27', '%22')
    #print(encoded_subst)
    request = Request(get_url, encoded_subst.encode())
    response = urlopen(request).read().decode()

    resp_json = json.loads(response)
    count_fails = 0
    count_success = 0
    for ret in resp_json['action_results']:
        if ret is True:
            count_success+=1
        elif ret is False:
            count_fails+=1
        else:
            print("result is %s" % str(ret))
            return

    print ("%d Success, %d Fails" % (count_success, count_fails))
