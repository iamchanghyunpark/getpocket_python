import json
import os.path
import requests
from urllib.parse import urlencode
from urllib.request import Request, urlopen, urlretrieve


def check_and_load():
    global consumer_key

    auth_filename = 'auth.json'
    consumer_key='Insert_yours_here' # The one you get from getpocket.com

    auth_credentials = {
            'consumer_key':None,
            'access_token':None
            }

    if os.path.exists(auth_filename):
        with open(auth_filename, 'r') as fp:
            loaded_auth = json.load(fp)
        if loaded_auth['consumer_key'] is not None:
            auth_credentials['consumer_key'] = loaded_auth['consumer_key']
        if loaded_auth['access_token'] is not None:
            auth_credentials['access_token'] = loaded_auth['access_token']
    else:
        auth_credentials['consumer_key'] = consumer_key

    if auth_credentials['access_token'] is None:
        auth_credentials = send_auth_request(auth_credentials)

    with open(auth_filename, 'w+') as fp:
        json.dump(auth_credentials, fp)

    return auth_credentials

def send_auth_request(cred):
    req_url = 'https://getpocket.com/v3/oauth/request'
    auth_url = 'https://getpocket.com/v3/oauth/authorize'
    user_auth_url = 'https://getpocket.com/auth/authorize'

    post_fields = {
            'consumer_key': cred['consumer_key'],
            'redirect_uri':'https://getpocket.com'
            }

    request = Request(req_url, urlencode(post_fields).encode())
    code_string = urlopen(request).read().decode()
    code = code_string.split('=')[1]
    #print(code)

    post_fields = {
            'request_token':code,
            'redirect_uri':'https://getpocket.com'
    }

    # User auth thingy
    print("User should click link below and authorize")
    print(user_auth_url + '?' + urlencode(post_fields))
    print("Insert any string after done authorizing on the web")
    input()

    post_fields = {'consumer_key': cred['consumer_key'],
            'code':code}

    request = Request(auth_url, urlencode(post_fields).encode())
    resp = urlopen(request).read().decode()

    resps = resp.split('&')
    for resp in resps:
        if 'access_token' in resp:
            cred['access_token'] = resp.split('=')[1]
            break
    #print(cred)
    return cred
