# getpocket_python

Python stuff for accessing the getpocket REST APIs.

## First setup the keys
Get your 'consumer key' from getpocket website.
Set the key in `consumer_key` in the `check_and_load` function which is in the `keys.py` file.
Then run

```
$ python3 get_auth.py
```

This will create a link on your terminal that you should visit with your browser and authorize the key.
Once you have authorized on your browser press 'enter' on the terminal
and now you should have your `auth.json` file that holds your keys.
Now you are ready to start using the API

## Calling a few API calls

There are a few methods in `get_list.py` which may help you write your own.
You should check out the [getpocket API](https://getpocket.com/developer/docs/getstarted/web) for more
sophisticated things. I only implemented retrieving the list, and modifying entries.

Currently `get_list.py` returns the total number of articles you have on getpocket.

```
$ python3 get_list.py
You have a total of 24802 articles
```
